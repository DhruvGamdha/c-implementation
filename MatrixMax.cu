#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <time.h>
#include <sys/time.h>
#include <iomanip>

using namespace std;

#define cudaCheckError() {																\
	cudaError_t e=cudaGetLastError();													\
	if(e!=cudaSuccess) {																\
		printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e));	\
		}																					\
}

class v3
{
    public:

    void setValue(double x, double y, double z);
    void printV3();

    double m_x, m_y, m_z;

};

void v3::setValue(double x, double y, double z)
{
    m_x = x;
    m_y = y;
    m_z = z;
}

void v3::printV3()
{
    cout << m_x << "  "<< m_y << "  "<< m_z << endl;
}

class tri
{
    public:

    void setValue(v3 p1, v3 p2, v3 p3);
    void printTri();

    v3 m_p1, m_p2, m_p3;
};

void tri::setValue(v3 po1, v3 po2, v3 po3)
{
    this->m_p1 = po1;
    this->m_p2 = po2;
    this->m_p3 = po3;
}

void tri::printTri()
{
    m_p1.printV3();
    m_p2.printV3();
    m_p3.printV3();
}


__device__ void crossProduct(v3 a, v3 b, v3 *ans) 
{   //0 = x, 1=y, 2=z
    ans->m_x = a.m_y*b.m_z - a.m_z*b.m_y;
    ans->m_y = a.m_z*b.m_x - a.m_x*b.m_z;
    ans->m_z = a.m_x*b.m_y - a.m_y*b.m_x;
}

__device__ double dotProduct(v3 a, v3 b) 
{   
    return a.m_x*b.m_x + a.m_y*b.m_y + a.m_z*b.m_z;
}

__device__ void addValue(double *ts_arr, int *ts_count, double t_val) 
{   
    t_val = t_val*(1e12);
    t_val = round(t_val);
    t_val = t_val*(1e-12);
    
    int count = 0;
    
    for(int i=0; i<*ts_count; i++)
    {
        if(ts_arr[i]==t_val)
        {   count = 1;
            break;
        }
    }
    
    if(count == 0)
    {
        ts_arr[*ts_count] = t_val;
        *ts_count = *ts_count + 1;
    }
}

__device__ void fillIntersecArr(v3 source, v3 dir, double ts_arr[], int ts_count, v3 intersecArr[]) 
{   
    for(int t_ind=0; t_ind<ts_count; t_ind++)
    {
        intersecArr[t_ind].m_x = source.m_x + ts_arr[t_ind]*dir.m_x;
        intersecArr[t_ind].m_y = source.m_y + ts_arr[t_ind]*dir.m_y;
        intersecArr[t_ind].m_z = source.m_z + ts_arr[t_ind]*dir.m_z;
    }
}

__device__ double rayTriangleIntersection(v3 p0, v3 p1, v3 p2, v3 source, v3 dir, bool *val) 
{   
    double epsilon = 1e-8;
    
    v3 p0p1;
    p0p1.m_x = p1.m_x - p0.m_x;
    p0p1.m_y = p1.m_y - p0.m_y;
    p0p1.m_z = p1.m_z - p0.m_z;
    
    v3 p0p2;
    p0p2.m_x = p2.m_x - p0.m_x;
    p0p2.m_y = p2.m_y - p0.m_y;
    p0p2.m_z = p2.m_z - p0.m_z;
    
    v3 pvec;
    crossProduct(dir, p0p2, &pvec);
    
    double det = 0;
    det = dotProduct(p0p1, pvec);
    
    if(det<epsilon && det>-epsilon)
    {
        *val = false;
        return 0;
    }
    
    double invDet = 1/det;
    
    v3 tvec; 
    tvec.m_x = source.m_x - p0.m_x;
    tvec.m_y = source.m_y - p0.m_y;
    tvec.m_z = source.m_z - p0.m_z;
    
    double u = dotProduct(tvec, pvec);
    u = u*invDet;
    
    if(u<0 || u>1)
    {
        *val = false;
        return 0;
    }
    
    v3 qvec;
    crossProduct(tvec, p0p1, &qvec);
    
    double v = dotProduct(dir, qvec);
    v = v*invDet;
    
    if(v<0 || u+v>1)
    {
        *val = false;
        return 0;
    }
    
    double t = dotProduct(p0p2, qvec);
    t = t*invDet;
    
    *val = true;
    return t;
}

__device__ double getLengthInMaterial(v3 intersectionsArray[], int N_intersec) 
{   
    double length = 0;
    for(int i=0; i<N_intersec; i=i+2)
    {
        length = length + sqrt(pow((intersectionsArray[i].m_x-intersectionsArray[i+1].m_x),2) + \
                            pow((intersectionsArray[i].m_y-intersectionsArray[i+1].m_y),2) + \
                            pow((intersectionsArray[i].m_z-intersectionsArray[i+1].m_z),2));
    }
    return length;
}

void getTargets(v3 targets[], double detec_dep, int pixels, double detec_size)
{
    double d = detec_size/pixels;
    for(int i=0; i<pixels; i++)
    {
        for(int j=0; j<pixels; j++)
        {
            targets[i*pixels+j].m_x = -(detec_size/2) + d/2 + j*d;
            targets[i*pixels+j].m_y = (detec_size/2) - d/2 - i*d;
            targets[i*pixels+j].m_z = -detec_dep;
        }
    }
}

__global__ void getDistanceArray_MT(tri *meshTri, int *meshSize, v3 *source, v3 *targets, double *distArray, int *pixels)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    int j = blockIdx.y*blockDim.y + threadIdx.y;

    if (i < *pixels && j < *pixels)
    {
        int index = i*(*pixels)+j;
        const int arrsize = 10;
        v3 intersecArray[arrsize];
        double ts[arrsize];
        int N_intersec = 0;
        v3 dir;
        
        dir.m_x = targets[index].m_x - source->m_x;
        dir.m_y = targets[index].m_y - source->m_y;
        dir.m_z = targets[index].m_z - source->m_z;
        
        bool tOrF = false;
        double intersection;
        for(int i=0; i<*meshSize; i++)
        {
            intersection  = rayTriangleIntersection(meshTri[i].m_p1, meshTri[i].m_p2, meshTri[i].m_p3, *source, dir, &tOrF);
            if(tOrF==true && N_intersec<arrsize)
            {
                addValue(ts, &N_intersec, intersection);
            }
        }
        
        for(int a=0; a<N_intersec; a++)
        {
            for(int b=a+1; b<N_intersec; b++)
            {
                if(ts[a]>ts[b])
                {
                    double temp = ts[a];
                    ts[a] = ts[b];
                    ts[b] = temp;
                }
            }
        }
        
        fillIntersecArr(*source, dir, ts, N_intersec, intersecArray);
        distArray[index] = getLengthInMaterial(intersecArray, N_intersec);
    }
}

void stlReadIntoVector(vector<tri> *vec, string filename)
{   /* 
    To read the STL file with name "filename" in a vector.
    */
    tri tricoord;
    v3 tempV3[3];
    double temp[3];
    string outerloop;
    string vertex;
    
    ifstream stlfile(filename);
    
    if ( !stlfile )
    {
        cout << "\n";
        cout << "STLA_READ - Fatal error!\n";
        cout << "  Could not open the file \"" << filename << "\".\n";
    }
    
    while(getline(stlfile, outerloop)){
        
        if (outerloop == "  outer loop")
        { 
            for(int i =0; i<3;i++)
            {
                stlfile >> vertex >> temp[0] >> temp[1] >> temp[2];
                if (vertex == "vertex")
                {
                    tempV3[i].setValue(temp[0],temp[1],temp[2]);
                }
            }
            tricoord.setValue(tempV3[0],tempV3[1],tempV3[2]);
            vec->push_back(tricoord);
        }
        
    }
}

void saveToCSV(string name, double * arr, int arrsize)
{   /*
    Stores the 2D array (arr) of size arrsize * arrsize as a CSV file. 
    */
    ofstream image (name);
    if (image.is_open())
    {
        for (int i=0; i<arrsize; i++)
        {
            for (int j=0; j<arrsize; j++)
            {
                image << arr[i*(arrsize)+j] << ",";
            }
            image << "\n";
        }
        image.close();
    }
    else cout << "Unable to open file";
}

int main()
{   
    vector<tri> stlMesh;    // using vector to read the data from the stl file. 
    string fname = "/content/notebooks/ME570X/raycast/C/Tubular_2mm_centered.stl"; // STL filename with location.
    struct timeval begin, end;          // Variables to time the computation. 
    
    gettimeofday(&begin, NULL);
    
    stlReadIntoVector(&stlMesh,fname);  // reading of the stl file into vector.
    
    gettimeofday(&end, NULL);
    double readSTLTime = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< " Reading STL file into vector Time : " << readSTLTime << endl;
    
    int totTriangles = stlMesh.size();  // storing the total number of triangles in the mesh.
    cout<< "totTriangles:   "<< totTriangles << endl;
    
    gettimeofday(&begin, NULL);
    tri* meshTriangles = new tri[totTriangles];     // creating array to store the mesh data.
    
    // Changing the datatype of mesh from vector to array for the further use. 
    for(int i=0; i<totTriangles; i++)
    {
        meshTriangles[i].m_p1 = stlMesh[i].m_p1;
        meshTriangles[i].m_p2 = stlMesh[i].m_p2;
        meshTriangles[i].m_p3 = stlMesh[i].m_p3;
    }
    
    gettimeofday(&end, NULL);
    double vecToArrcopyTime = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< "Vector to Array mesh copying Time : " << vecToArrcopyTime << endl;
    
    // setting virtual experimental setup parameters. 
    double detectorDepth = 200;     // negative of the z coordinate of the detector plane, detector plane is perpendicular to z-axis. 
    const int pixels = 2048;        // Image resolution. 
    double detectorSize = 500;      // length and width of the detector. The detector is a square panel. 
    
    v3 source;                      // source coordinates.
    source.setValue(0,0,500);       
    
    double* distanceArr= new double[pixels*pixels];     // array to store the distance values of the ray inside component.
    
    v3* targets = new v3[pixels*pixels];        // array to store the target coordinates for the rays.
    getTargets(targets,detectorDepth, pixels, detectorSize);
    
    dim3 block(8, 8, 1);                        // Block dimension, threads per block.
    dim3 grid(256, 256, 1);                     // Grid dimension, blocks per grid.
    
    // Allocating and copying mesh data inside the GPU.
    tri *meshTrianglesCUDA;                     
    cudaMalloc((void**)&(meshTrianglesCUDA), totTriangles*sizeof(tri));
    cudaMemcpy(meshTrianglesCUDA, meshTriangles, totTriangles*sizeof(tri), cudaMemcpyHostToDevice);
    
    // Allocating and copying source coordinates inside GPU.
    v3 *sourceCUDA;
    cudaMalloc((void**)&(sourceCUDA), sizeof(v3));
    cudaMemcpy(sourceCUDA, &source, sizeof(v3), cudaMemcpyHostToDevice);
    
    // Allocating and copying target values inside the GPU.
    v3 *targetsCUDA;
    cudaMalloc((void**)&(targetsCUDA), pixels*pixels*sizeof(v3));
    cudaMemcpy(targetsCUDA, targets, pixels*pixels*sizeof(v3), cudaMemcpyHostToDevice);
    
    // Allocating empty array inside the GPU to store the values the distance travelled by the rays.
    double *distanceArrCUDA;
    cudaMalloc((void**)&(distanceArrCUDA), pixels*pixels*sizeof(double));
    
    // Allocating and copying the number of the pixels in the detector inside the GPU. 
    int *pixelsCUDA;
    cudaMalloc((void**)&(pixelsCUDA), sizeof(int));
    cudaMemcpy(pixelsCUDA, &pixels, sizeof(int), cudaMemcpyHostToDevice);
    
    // Allocating and copying the total number of triangles inside the GPU.
    int *totTrianglesCUDA;
    cudaMalloc((void**)&(totTrianglesCUDA), sizeof(int));
    cudaMemcpy(totTrianglesCUDA, &totTriangles, sizeof(int), cudaMemcpyHostToDevice);
    
    gettimeofday(&begin, NULL);
    
    // Executing the kernel.
    getDistanceArray_MT <<< grid, block >>>(meshTrianglesCUDA, totTrianglesCUDA, sourceCUDA, targetsCUDA, distanceArrCUDA, pixelsCUDA);
    
    // Copying the distance values from the GPU to CPU.
    cudaMemcpy(distanceArr, distanceArrCUDA, pixels*pixels*sizeof(double), cudaMemcpyDeviceToHost);
    
    gettimeofday(&end, NULL);
    double GPUtime_P1 = (end.tv_sec - begin.tv_sec) + 1e-6 * (end.tv_usec - begin.tv_usec);
    cout << fixed << showpoint;
    cout << setprecision(7);
    cout<< "Pattern 1 GPU Time : " << GPUtime_P1 << endl;
    
    // Storing the distance values in depth.csv file.
    string csvname = "/content/notebooks/ME570X/raycast/C/depth.csv";
    saveToCSV(csvname, distanceArr, pixels);
    
    // freeing the memory.
    delete[] distanceArr;
    delete[] targets;
    
    return 0;
}

///////////// Debug ///////////////////
    /*
    cout << "Before copying targets[10].printV3()"<< endl;
    targets[10].printV3();
    
    targets[10].m_x = 999999999;
    cout<< "Made Changes in m_x value"<< endl;
    targets[10].printV3();
    cudaMemcpy(targets, targetsCUDA, pixels*pixels*sizeof(v3), cudaMemcpyDeviceToHost);
    cout << "Copy back targets[10].printV3();"<< endl;
    targets[10].printV3();
    
    cout<< "totTriangles*sizeof(tri):   "<< totTriangles*sizeof(tri)<< endl;
    cout<< "sizeof(meshTriangles):  " <<sizeof(meshTriangles)<< endl;
    cout<< "sizeof(tri):    "<< sizeof(tri)<< endl;
    */
    ////////////////////////////////////////////